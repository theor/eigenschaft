﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Nancy;
using Nancy.Hosting.Self;

namespace EigenschaftServer
{
    class Program
    {
        static void Main()
        {
            var uri = new Uri("http://localhost:1234");
            using (var host = new NancyHost(uri))
            {
                host.Start();
                Console.WriteLine("Hosting server at {0}", uri);
                Console.ReadLine();
            }
        }
    }
    public class CustomRootPathProvider : IRootPathProvider
    {
        public string GetRootPath()
        {
            return Debugger.IsAttached ? Path.GetFullPath(".") : Assembly.GetEntryAssembly().Location;
        }
    }
    public class CustomBootstrapper : DefaultNancyBootstrapper
    {
        protected override IRootPathProvider RootPathProvider
        {
            get { return new CustomRootPathProvider(); }
        }
    }
    public class ResourceModule : NancyModule
    {
        public ResourceModule()
        {
            // would capture routes to /products/list sent as a GET request
            Get["/"] = _ => View["index.html"];
            Get["/features"] = parameters => File.ReadAllText("server.json");
        }
    }
}
