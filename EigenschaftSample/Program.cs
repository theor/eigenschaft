﻿using System;
using System.Collections.Generic;
using EigenschaftCore;

namespace EigenschaftSample
{
    [Features.FeaturesContainerAttribute]
    class Program
    {
        static Feature<bool> A = new Feature<bool>{Default = true};
        static Feature<int> B = null;
        static Feature<int> B2 = null;
        static Feature<string> C = new Feature<string>{Default = "DEFAULT"};

        static void Main(string[] args)
        {
            {
                List<IFeatureValue> values = new List<IFeatureValue>
                {
                    Features.MakeValue<Program, int>(_ => B, 42, constraints:new UsernameConstraint{Username = "user1"}),
                    Features.MakeValue<Program, int>(_ => B, 43, constraints:new UsernameConstraint{Username = "user2"}),
                    Features.MakeValue<Program, int>(_ => B2, 1),
                    //Features.MakeValue<C, string>("asda"),
                };
                FeatureProviders jp = new FeatureProviders("user.json");
                jp.Save(values);
            }
            
            {
                var mockDataProvider = new MockDataProvider(){Username = "user1"};
                Features.Initialize(mockDataProvider, new FeatureProviders("user.json"), new RestJsonProvider("http://127.0.0.1:1234/features"));
                
                Features.Load();
                var features = Features.GetTypeFeatures(typeof (Program));
                Console.WriteLine(mockDataProvider);
                foreach (IFeature f in features)
                {
                    Console.WriteLine("{0}:{1}", f.Name, f.ObjectValue);
                }


                mockDataProvider.Username = "user2";
                Features.Load();
                Console.WriteLine(mockDataProvider);
                foreach (IFeature f in features)
                {
                    Console.WriteLine("{0}:{1}", f.Name, f.ObjectValue);
                }

                Console.ReadLine();
            }

        }
    }
}
