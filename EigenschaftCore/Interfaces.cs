using System;
using System.Collections.Generic;

namespace EigenschaftCore
{
    public interface IFeature
    {
        string Name { get; }
        object ObjectValue { get; }
    }
    public interface IFeatureValue
    {
        string Name { get; }
        List<IConstraint> Constraints { get; }
        Features.Value Forced { get; }
        bool IsValid(IDataProvider dataProvider);
    }
    public interface IFeatureProvider
    {
        bool AllowForcedValues { get; }

        FeatureValue<T> Get<T>(IDataProvider dataProvider, Feature<T> feature);
        List<FeatureValue<T>> GetAll<T>(Feature<T> feature);
        void Load();
    }
    public interface IDataProvider
    {
        Version Version { get; }
        string Username { get; }
        string Machine { get; }
        DateTime Date { get; }
    }
    public interface IConstraint
    {
        bool IsValid(IDataProvider dataProvider);
    }
}