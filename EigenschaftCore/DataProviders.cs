using System;
using System.Reflection;

namespace EigenschaftCore
{

    public class MockDataProvider : IDataProvider
    {
        public Version Version { get; set; }
        public string Username { get; set; }
        public string Machine { get; set; }
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return string.Format("Version: {0}, Username: {1}, Machine: {2}, Date: {3}", Version, Username, Machine, Date);
        }
    }

    public class DataProvider : IDataProvider
    {
        public Version Version { get { return Assembly.GetEntryAssembly().GetName().Version; } }
        public string Username { get { return Environment.UserName; } }
        public string Machine { get { return Environment.MachineName; } }
        public DateTime Date { get { return DateTime.Now; } }
    }
}