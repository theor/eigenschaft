using System.Collections.Generic;
using System.Linq;

namespace EigenschaftCore
{
    public class Feature<T> : IFeature
    {
        private FeatureValue<T> _value;
        public virtual T Default { get; set; }

        internal FeatureValue<T> FeatureValue
        {
            get { return _value ?? (_value = Features.GetValue(this)); }
        }

        public T Value
        {
            get { return FeatureValue.Value; }
        }

        public string Name { get; internal set; }
        public object ObjectValue { get { return Value; } }
    }

    public class FeatureValue<TU> : IFeatureValue
    {
        private List<IConstraint> _constraints = new List<IConstraint>();
        public string Name { get; set; }
        public TU Value { get; set; }

        public List<IConstraint> Constraints
        {
            get { return _constraints; }
            set { _constraints = value; }
        }

        public Features.Value Forced { get; set; }

        public bool IsValid(IDataProvider dataProvider)
        {
            return Constraints.All(c => c.IsValid(dataProvider));
        }
    }
}