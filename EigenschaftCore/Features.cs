﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace EigenschaftCore
{
    public class Features
    {
        [AttributeUsage(AttributeTargets.Class|AttributeTargets.Assembly)]
        public class FeaturesContainerAttribute : Attribute{}
        public enum Value
        {
            None,
            Forced
        }
        private static readonly List<IFeatureProvider> Providers = new List<IFeatureProvider>();
        private static IDataProvider _dataProvider;

        public static void Initialize(IDataProvider dataProvider, params IFeatureProvider[] providers)
        {
            if(_dataProvider != null)
                throw new InvalidOperationException("Already");
            _dataProvider = dataProvider;
            Providers.AddRange(providers);
        }

        public static T Get<T>(Feature<T> feature)
        {
            var value = GetValue(feature);
            //typeof(T).GetProperty("Value", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy).SetValue(null, value);
            return value.Value;
        }

        public static FeatureValue<T> GetValue<T>(Feature<T> feature)
        {
            if (_dataProvider == null)
                throw new InvalidOperationException("Need to call Features.Initialize()");

            FeatureValue<T> value =
                Providers.Select(p => p.Get(_dataProvider, feature))
                    .Where(v => v != null)
                    .OrderBy(x => x.Forced == Value.Forced ? -1 : 1)
                    .FirstOrDefault();
            return value ?? MakeValue(feature.Name, feature.Default);
        }

        public static void AddProvider(IFeatureProvider featureProvider)
        {
            Providers.Add(featureProvider);
        }

        public static void Load()
        {
            Task.WaitAll(Providers.Select(p => Task.Run(() => p.Load())).ToArray());
            ApplyFeatureValues();
        }

        private static void ApplyFeatureValues()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if(assembly.GetCustomAttribute<FeaturesContainerAttribute>() != null)
                    continue;
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    List<FieldInfo> featuresProps = GetTypeFeatureMembers(type);
                    if(featuresProps == null || !featuresProps.Any())
                        continue;

                    foreach (var featuresProp in featuresProps)
                    {
                        string featureName = MakeFeatureName(featuresProp);
                       
                        var x = featuresProp.GetValue(null);
                        if (x == null)
                        {
                            x = Activator.CreateInstance(featuresProp.FieldType);
                            featuresProp.SetValue(null, x);
                        }
                        var nameProp = featuresProp.FieldType.GetProperty("Name", BindingFlags.Instance | BindingFlags.Public);
                        Debug.Assert(nameProp != null, "nameProp != null");
                        nameProp.SetValue(x, featureName);

                        var valueProp = featuresProp.FieldType.GetField("_value", BindingFlags.Instance | BindingFlags.NonPublic);
                        Debug.Assert(valueProp != null, "valueProp != null");
                        valueProp.SetValue(x, null);

                        Trace.WriteLine(x);
                    }
                }
            }
        }

        public static List<IFeature> GetTypeFeatures(Type type)
        {
            var featuresProps = GetTypeFeatureMembers(type);
            if (featuresProps == null) return null;
            return featuresProps.Select(x => x.GetValue(null)).OfType<IFeature>().ToList();
        }

        private static List<FieldInfo> GetTypeFeatureMembers(Type type)
        {
            if (type.CustomAttributes.All(a => a.AttributeType != typeof (FeaturesContainerAttribute)))
                return null;
            var featuresProps =
                type.GetFields(BindingFlags.Static | BindingFlags.NonPublic)
                    .Where(p => typeof (IFeature).IsAssignableFrom(p.FieldType)).ToList();
            return featuresProps;
        }

        private static string MakeFeatureName(MemberInfo featuresProp)
        {
            Debug.Assert(featuresProp.DeclaringType != null, "featuresProp.DeclaringType != null");
            return featuresProp.DeclaringType.FullName + "." + featuresProp.Name;
        }

        public static FeatureValue<TFeature> MakeValue<TContainer, TFeature>(Expression<Func<TContainer, Feature<TFeature>>> exp, TFeature value, Value forced = Value.None, params IConstraint[] constraints)
        {
            var body = exp.Body as MemberExpression;
            Debug.Assert(body != null, "body != null");
            string featureName = MakeFeatureName(body.Member);
            return MakeValue(featureName, value, forced, constraints);
        }

        private static FeatureValue<T> MakeValue<T>(string featureName, T value, Value forced = Value.None, params IConstraint[] constraints)
        {
            return new FeatureValue<T> {Name = featureName, Value = value, Forced = forced, Constraints = constraints != null ? constraints.ToList() : new List<IConstraint>()};
        }
    }
}
