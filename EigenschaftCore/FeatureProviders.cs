using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using RestSharp;

namespace EigenschaftCore
{
    public abstract class JsonProviderBase : IFeatureProvider
    {
        public bool AllowForcedValues { get; set; }

        private List<IFeatureValue> _values;

        protected static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto
        };

        
        public FeatureValue<T> Get<T>(IDataProvider dataProvider, Feature<T> feature)
        {
            return GetAll(feature).FirstOrDefault(f => f.IsValid(dataProvider));
        }

        public List<FeatureValue<T>> GetAll<T>(Feature<T> feature)
        {
            List<FeatureValue<T>> featureValues = _values.Where(x => x.Name == feature.Name).OfType<FeatureValue<T>>().ToList();
            return featureValues;
        }

        public void Load()
        {
            string json = GetJson();
            _values = String.IsNullOrWhiteSpace(json) ? new List<IFeatureValue>() : JsonConvert.DeserializeObject<List<IFeatureValue>>(json, JsonSerializerSettings);
        }

        protected abstract string GetJson();
    }

    public class RestJsonProvider : JsonProviderBase
    {
        private readonly RestClient _client;

        public RestJsonProvider(string url)
        {
            _client = new RestClient(url);
        }

        protected override string GetJson()
        {
            var req = new RestRequest("");
            var response = _client.Execute(req);
            return response.Content;
        }
    }

    public class FeatureProviders : JsonProviderBase
    {
        private readonly string _featuresJson;

        public FeatureProviders(string featuresJson)
        {
            _featuresJson = featuresJson;
        }

        protected override string GetJson() 
        {
            if (!File.Exists(_featuresJson))
                return null;
            return File.ReadAllText(_featuresJson);
        }

        public void Save(List<IFeatureValue> values)
        {
            var json = JsonConvert.SerializeObject(values, Formatting.Indented, JsonSerializerSettings);
            File.WriteAllText(_featuresJson, json);
        }
    }
}