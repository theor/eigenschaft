using System;

namespace EigenschaftCore
{
    public class UsernameConstraint : IConstraint
    {
        public string Username { get; set; }
        public bool IsValid(IDataProvider dataProvider)
        {
            return Username == null || Username == dataProvider.Username;
        }
    }
    internal class MachineConstraint : IConstraint
    {
        public string Machine { get; set; }
        public bool IsValid(IDataProvider dataProvider)
        {
            return Machine == null || Machine == dataProvider.Machine;
        }
    }
    internal class VersionConstraint : IConstraint
    {
        public Version MinVersion { get; set; }
        public Version MaxVersion { get; set; }

        public bool IsValid(IDataProvider dataProvider)
        {
            Version version = dataProvider.Version;
            return (MinVersion == null || MinVersion <= version) && (MaxVersion == null || version <= MaxVersion);
        }
    }
    internal class DateConstraint : IConstraint
    {
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }

        public bool IsValid(IDataProvider dataProvider)
        {
            DateTime version = dataProvider.Date;
            return (MinDate == null || MinDate <= version) && (MaxDate == null || version <= MaxDate);
        }
    }
}